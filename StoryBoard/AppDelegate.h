//
//  AppDelegate.h
//  StoryBoard
//
//  Created by Click Labs134 on 9/23/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

